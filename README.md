## Authentication module for Smorken/App

Licensed under the [MIT License](http://opensource.org/licenses/MIT)

### Standalone use ###

See examples/standalone

### Use with simple app project

See examples

Create an app based on [scott_morken/app-module](https://bitbucket.org/scott_morken/app-module)

Add the auth service to **config/app.php**

```
'Pc\Auth\AuthService'
```

Add the composer information for the auth module to **composer.json**

~~~~
...
"repositories": [
    {
      "url": "https://scott_morken@bitbucket.org/scott_morken/app-auth.git",
      "type": "git"
    },
    {
      "url": "https://scott_morken@bitbucket.org/scott_morken/app-utils.git",
      "type": "git"
    },
    {
      "url": "https://scott_morken@bitbucket.org/scott_morken/app-session.git",
      "type": "git"
    },
    {
      "url": "https://scott_morken@bitbucket.org/scott_morken/app-config.git",
      "type": "git"
    }
],
"require": {
    "smorken/auth": "~1.0"
},
...
~~~~

Update the packages via [composer](https://getcomposer.org/doc/00-intro.md)

```
$ composer install
```

Add (copy from the vendor) the **config/auth.php** file

**config/auth.php** (for ad/ldap backend)

~~~~
<?php
return array(
    'backend' => array(
        'class' => '\Smorken\Auth\Backend\AdBackend',
        'options' => array(
            'account_suffix' => '@domain.org',
            'domain_controllers' => array('ldap.mycollege.edu'),
            'base_dn' => 'ou=employees,dc=mycollege,dc=org',
            'admin_username' => 'myuser',
            'admin_password' => 'password',
            'real_primary_group' => true,
            'use_ssl' => true,
            'use_tls' => false,
            'recursive_groups' => false,
            'fields' => array(
                'username' => 'samaccountname',
                'last_name' => 'sn',
                'first_name' => 'givenname',
                'email' => 'mail',
                'phone' => 'telephonenumber',
                'groups' => true,
            )
        )
    ),
    'require_ssl' => true,
    'return_url_key' => 'returnUrl',
    'login_view' => null,
    'login_url' => null,
    'logout_key' => 'logout',
    /*
     * example of setting auth requirements for legacy routes
     */
    /*
    'requires_auth' => array(
        'forms.php',
        'secure/',
        'assessments.php',
        'reports.php',
        'reports/',
        'forms/',
        'ajax/',
    ),
    */
);
~~~~

Add something like the following to the **bootstrap/start.php** file

**bootstrap/start.php**

~~~~
$app->start();
...
/* 
 * uses requires_auth key from config/auth.php to control when authentication is required
 * requires view service and login view
 * another option is in the vendor auth public directory.  no views required.
 */
$requires_auth = $app['config']->get('auth.requires_auth', array());
$s = $_SERVER['PHP_SELF'];
if (!$app['auth']->isAuthenticated()) {
    foreach ($requires_auth as $path) {
        if (stristr($s, $path) !== false) {
            $app['auth']->setReturnUrl($s);
            $app['view']->render('login', array('page_title' => 'Log In'));
            exit(0);
        }
    }
}
...
//router service
return $app;
~~~~

The login view (if you use that option), should post to a route that contains logic similar to:

~~~~
<?php
$app = Smorken\Application\App::getInstance();
if (!$app['auth']->isAuthenticated()) {
    $authenticated = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $post = filter_input_array(INPUT_POST, array(
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
            'redirectTo' => FILTER_SANITIZE_URL,
        ));
        $app['auth']->setReturnUrl($post['redirectTo']);
        $authenticated = $app['auth']->authenticate($post['username'], $post['password']);
    }
    if (!$authenticted) {
        $app['view']->render('login', array('errors' => $app['auth']->getErrors());
    }
}
~~~~

## Extending

You can create a new backend by minimally implementing Pc\Auth\Backend\IBackend.

You can extend Pc\Auth\Backend\AbstractBackend if its functionality works for you but you 
will still need to implement the interface.

Check out Pc\Auth\Backend\DummyBackend and AdBackend for examples of how they work (and their respective
tests in the tests directory).

The base user class (GenericUser) is a simple object that takes the attributes provided by the 
authentication adapter and the config and returns a simple object that has access to those attributes. It
is up to you to provide the user class name in your auth backend. 

TODO: move class name out of backend and into config...

You can extend it to handle your own domain specific user logic, etc.  Make sure to implement Pc\Auth\Model\IUser in your
class.  You can also simply use the default GenericUser and do whatever is needed outside of
the authentication engine.