<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 1:11 PM
 */

use \Smorken\Auth\AuthHandler;

class AuthHandlerTest extends PHPUnit_Framework_TestCase {

    /**
     * @var AuthHandler
     */
    protected $SUT;

    public function setUp()
    {
        $config = $this->getConfig();
        $session = $this->getSession();
        $this->SUT = new AuthHandler($config, $session);
    }

    public function testGetConfig()
    {
        $this->assertInstanceOf('\Smorken\Config\Config', $this->SUT->getConfig());
    }

    public function testGetBackend()
    {
        $this->assertInstanceOf('\Smorken\Auth\Backend\IBackend', $this->SUT->getBackend());
    }

    public function testGetSession()
    {
        $this->assertInstanceOf('\Smorken\Session\SessionHandler', $this->SUT->getSession());
    }

    public function testIsAuthenticated()
    {
        $user = new \Smorken\Auth\Model\GenericUser(array('id' => 2, 'username' => 'test2'));
        $userserial = serialize($user);
        $this->SUT->getSession()->user = $userserial;
        $this->assertTrue($this->SUT->isAuthenticated());
        $this->assertEquals(2, $this->SUT->getBackend()->getUser()->getId());
    }

    public function testUser()
    {
        $this->assertInstanceOf('\Smorken\Auth\Model\IUser', $this->SUT->user());
    }

    public function testAuthenticate()
    {
        $this->assertTrue($this->SUT->authenticate('test', 'test'));
        $this->assertEquals(1, $this->SUT->user()->getId());
    }

    public function testHasErrors()
    {
        $this->assertFalse($this->SUT->hasErrors());
    }

    public function testIsAuthenticatedTrueWhenNoHandler()
    {
        $config = $this->getConfig();
        $config['auth.authenticate'] = false;
        $session = $this->getSession();
        $sut = new AuthHandler($config, $session, null);
        $this->assertTrue($sut->isAuthenticated());
    }

    protected function getConfig()
    {
        $loader = new \Smorken\Config\FileLoader(__DIR__ . '/../../config');
        return new \Smorken\Config\Config($loader);
    }

    protected function getSession()
    {
        return new \Smorken\Session\SessionHandler(new \Smorken\Session\Adapter\ObjectAdapter());
    }
}
 