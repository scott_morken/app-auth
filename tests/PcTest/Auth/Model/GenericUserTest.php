<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:10 AM
 */

use \Smorken\Auth\Model\GenericUser;

class GenericUserTest extends PHPUnit_Framework_TestCase {

    public function testGetIdIsNull()
    {
        $user = new GenericUser();
        $this->assertNull($user->getId());
    }

    public function testGetUsernameIsNull()
    {
        $user = new GenericUser();
        $this->assertNull($user->getUsername());
    }

    public function testGetAttribute()
    {
        $user = new GenericUser();
        $this->assertNull($user->getValue('foo'));
    }

    public function testSetAttribute()
    {
        $user = new GenericUser();
        $user->setValue('foo', 'bar');
        $this->assertEquals('bar', $user->getValue('foo'));
    }

    public function testMagicSet()
    {
        $user = new GenericUser();
        $user->foo = 'bar';
        $this->assertEquals('bar', $user->getValue('foo'));
    }

    public function testMagicGet()
    {
        $user = new GenericUser();
        $user->setValue('foo', 'bar');
        $this->assertEquals('bar', $user->foo);
    }

    public function testSetFromArray()
    {
        $data = $this->getUserData();
        $user = new GenericUser();
        $user->setFromArray($data);
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('user1', $user->getUsername());
    }

    public function testSetFromArrayConstructor()
    {
        $user = new GenericUser($this->getUserData());
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('user1', $user->getUsername());
    }

    /**
     * @expectedException \Smorken\Auth\Model\UserException
     */
    public function testSetFromArrayException()
    {
        $data = $this->getUserData();
        unset($data['id']);
        $user = new GenericUser();
        $user->setFromArray($data);
    }

    public function testGetArray()
    {
        $user = new GenericUser($this->getUserData());
        $this->assertEquals($this->getUserData(), $user->toArray());
    }

    protected function getUserData()
    {
        $data = array(
            'id' => 1,
            'username' => 'user1'
        );
        return $data;
    }
}
 