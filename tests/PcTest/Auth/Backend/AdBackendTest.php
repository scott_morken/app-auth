<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/7/14
 * Time: 9:07 AM
 */

use \Smorken\Auth\Backend\AdBackend;

class AdBackendTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Auth\Backend\AdBackend
     */
    private $SUT;
    /**
     * @var array
     */
    private $credentials;
    /**
     * @var \Adldap\Adldap
     */
    private $ad;
    /**
     * @var \Smorken\Auth\Model\GenericUser
     */
    private $user;
    private $ident;

    public function setUp()
    {
        $this->credentials = array('username' => 'user', 'password' => 'password');

        $this->ident = $this->credentials['username'];

        $this->ad = $this->getMockBuilder('Adldap\Adldap')
            ->disableOriginalConstructor()
            ->setMethods(array('user', 'authenticate', 'infoCollection'))
            ->getMock();

        $this->ad->expects($this->any())
            ->method('user')
            ->will($this->returnValue($this->ad));

        $this->user = $this->getMockBuilder('\Smorken\Auth\Model\GenericUser')
            ->disableOriginalConstructor()
            ->getMock();

        $this->SUT = new AdBackend();
    }

    public function testAuthenticateValid()
    {
        $this->ad->expects($this->once())
            ->method('authenticate')
            ->will($this->returnValue(true));

        $this->ad->expects($this->once())
            ->method('infoCollection')
            ->with($this->ident, array('*'))
            ->will($this->returnValue($this->getLdapInfo()));

        $this->SUT->initOptions($this->getConfig(), $this->ad);

        $this->assertTrue($this->SUT->authenticate($this->credentials['username'], $this->credentials['password']));
        $this->assertEquals($this->credentials['username'], $this->SUT->getUser()->getId());
    }

    public function testAuthenticateInvalid()
    {
        $this->ad->expects($this->once())
            ->method('authenticate')
            ->will($this->returnValue(false));
        $this->SUT->initOptions($this->getConfig(), $this->ad);
        $this->assertFalse($this->SUT->authenticate($this->credentials['username'], $this->credentials['password']));
        $this->assertEquals(1, count($this->SUT->getErrors()));
    }

    protected function getConfig()
    {
        return array(
            'account_suffix' => '@domain.org',
            'domain_controllers' => array('somehost.domain.org'),
            'base_dn' => 'ou=container,dc=domain,dc=org',
            'admin_username' => 'admin_user',
            'admin_password' => 'admin_pw',
            'real_primary_group' => true,
            'use_ssl' => true,
            'use_tls' => false,
            'recursive_groups' => false,
        );
    }

    protected function getLdapInfo()
    {
        $info = new stdClass;

        $info->samaccountname = $this->credentials['username'];
        $info->displayName = 'Sam Accountname';
        $info->distinguishedname = 'DC=LDAP,OU=AUTH,OU=FIRST GROUP';
        $info->memberof = array(
            'CN=Group,OU=AUTH,OU=FIRST GROUP'
        );

        return $info;
    }
}
 