<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:36 AM
 */

use \Smorken\Auth\Backend\DummyBackend;

class DummyBackendTest extends PHPUnit_Framework_TestCase {
    public function testAuthenticate()
    {
        $backend = new DummyBackend();
        $r = $backend->authenticate('test', 'test');
        $this->assertEquals(1, $backend->getUser()->getId());
    }

    public function testLogin()
    {
        $user = new \Smorken\Auth\Model\GenericUser(array('id' => 1, 'username' => 'test1'));
        $backend = new DummyBackend();
        $backend->login($user);
        $this->assertEquals($user, $backend->getUser());
    }

    public function testLogout()
    {
        $user = new \Smorken\Auth\Model\GenericUser(array('id' => 1, 'username' => 'test1'));
        $backend = new DummyBackend();
        $backend->login($user);
        $this->assertTrue($backend->isAuthenticated());
        $backend->logout();
        $this->assertFalse($backend->isAuthenticated());
    }

    public function testGetErrors()
    {
        $backend = new DummyBackend();
        $errors = $backend->getErrors();
        $this->assertTrue(is_array($errors));
    }

    public function testIsAuthenticatedIsFalse()
    {
        $backend = new DummyBackend();
        $this->assertFalse($backend->isAuthenticated());
    }

    public function testIsAuthenticatedIsTrue()
    {
        $backend = new DummyBackend();
        $backend->authenticate('test', 'test');
        $this->assertTrue($backend->isAuthenticated());
    }

    public function testGetUser()
    {
        $backend = new DummyBackend();
        $backend->authenticate('test', 'test');
        $this->assertInstanceOf('\Smorken\Auth\Model\IUser', $backend->getUser());
        $this->assertEquals(1, $backend->getUser()->getId());
    }

    public function testOverrideUserClass()
    {
        $backend = new DummyBackend();
        $backend->initOptions(array('userClass' => '\stdClass'));
        $this->assertInstanceOf('stdClass', $backend->getUser());
    }
}
 