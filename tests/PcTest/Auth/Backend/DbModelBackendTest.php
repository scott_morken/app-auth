<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/7/14
 * Time: 9:07 AM
 */

use \Smorken\Auth\Backend\DbModelBackend;

class DbModelBackendTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Auth\Backend\AdBackend
     */
    private $SUT;
    /**
     * @var array
     */
    private $credentials;
    /**
     * @var \Smorken\Db\Contracts\Models\Model
     */
    private $model;
    /**
     * @var \Smorken\Utils\Hash\Contract\Hash
     */
    private $hasher;
    /**
     * @var \Smorken\Auth\Model\GenericUser
     */
    private $user;
    private $ident;

    public function setUp()
    {
        $this->credentials = array('username' => 'foo', 'password' => 'bar');

        $this->ident = $this->credentials['username'];

        $this->model = $this->getMockBuilder('Smorken\Db\Contracts\Models\Model')
            ->disableOriginalConstructor()
            ->setMethods(array('findByAttributes'))
            ->getMock();

        $this->hasher = $this->getMockBuilder('Smorken\Utils\Hash\Contracts\Hash')
            ->disableOriginalConstructor()
            ->setMethods(array('hash'))
            ->getMock();

        $this->hasher->expects($this->any())
            ->method('hash')
            ->will($this->returnValue('bar'));

        $this->user = $this->getMockBuilder('\Smorken\Auth\Model\GenericUser')
            ->disableOriginalConstructor()
            ->getMock();

        $this->SUT = new DbModelBackend();
    }

    public function testAuthenticateValid()
    {
        $this->model->expects($this->any())
            ->method('findByAttributes')
            ->will($this->returnValue($this->getUserModel()));
        $this->SUT->initOptions($this->getConfig(), $this->model, $this->hasher);
        $this->assertTrue($this->SUT->authenticate($this->credentials['username'], $this->credentials['password']));
        $this->assertEquals($this->credentials['username'], $this->SUT->getUser()->getUsername());
    }

    public function testAuthenticateInvalid()
    {
        $this->model->expects($this->any())
            ->method('findByAttributes')
            ->will($this->returnValue(null));
        $this->SUT->initOptions($this->getConfig(), $this->model, $this->hasher);
        $this->assertFalse($this->SUT->authenticate($this->credentials['username'], $this->credentials['password']));
        $this->assertEquals(1, count($this->SUT->getErrors()));
    }

    protected function getConfig()
    {
        return array(
            'account_suffix' => '@domain.org',
            'domain_controllers' => array('somehost.domain.org'),
            'base_dn' => 'ou=container,dc=domain,dc=org',
            'admin_username' => 'admin_user',
            'admin_password' => 'admin_pw',
            'real_primary_group' => true,
            'use_ssl' => true,
            'use_tls' => false,
            'recursive_groups' => false,
        );
    }

    protected function getUserModel()
    {
        $user = new stdClass;
        $user->id = 1;
        $user->username = 'foo';
        $user->password = 'bar';
        return $user;
    }
}
 