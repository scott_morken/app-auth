<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:03 AM
 */

namespace Smorken\Auth\Model;

/**
 * Class GenericUser
 * @package Smorken\Auth\Model
 *
 * Generic User model allows any property
 * to be set and used
 */
class GenericUser implements IUser
{
    protected $attributes = array();

    const ID_FIELD = 'id';
    const USERNAME_FIELD = 'username';

    public function __construct($user = array())
    {
        $this->attributes[self::ID_FIELD] = null;
        $this->attributes[self::USERNAME_FIELD] = null;
        if (!empty($user)) {
            $this->setFromArray($user);
        }
    }

    public function __get($name)
    {
        return $this->getValue($name);
    }

    public function __set($key, $value)
    {
        $this->setValue($key, $value);
    }

    public function getId()
    {
        return $this->attributes[self::ID_FIELD];
    }

    public function getUsername()
    {
        return $this->attributes[self::USERNAME_FIELD];
    }

    public function getValue($key)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }
        return null;
    }

    public function setValue($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function toArray()
    {
        return $this->attributes;
    }

    public function setFromArray($array)
    {
        foreach($array as $key => $value)
        {
            $this->setValue($key, $value);
        }
        if ($this->getId() === null) {
            throw new UserException("User id field " . self::ID_FIELD . " cannot be null.");
        }
        if ($this->getUsername() === null) {
            throw new UserException("User username field " . self::USERNAME_FIELD . " cannot be null.");
        }
    }
}