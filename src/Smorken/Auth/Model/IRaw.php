<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 1/13/15
 * Time: 7:08 AM
 */

namespace Smorken\Auth\Model;


interface IRaw {

    public function get();

    public function set($raw);

}