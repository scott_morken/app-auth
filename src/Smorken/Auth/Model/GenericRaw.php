<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 1/13/15
 * Time: 7:09 AM
 */

namespace Smorken\Auth\Model;


class GenericRaw implements IRaw {

    protected $raw;

    public function get()
    {
        return $this->raw;
    }

    public function set($raw)
    {
        $this->raw = $raw;
    }
}