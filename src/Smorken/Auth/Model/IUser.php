<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:02 AM
 */

namespace Smorken\Auth\Model;


interface IUser {
    /**
     * Returns the user's id
     * @return string
     */
    public function getId();

    /**
     * Returns the username
     * @return string
     */
    public function getUsername();

    /**
     * Magic method to get $name from the user model
     * @param $name
     * @return mixed
     */
    public function __get($name);

    /**
     * Magic method to set $key to $value on the user model
     * @param $key
     * @param $value
     * @return mixed
     */
    public function __set($key, $value);

    /**
     * Converts the user model to an array
     * @return array
     */
    public function toArray();
} 