<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 2:04 PM
 */

namespace Smorken\Auth;

use Smorken\Auth\Backend\AuthProxyBackend;
use Smorken\Auth\Backend\DbModelBackend;
use Smorken\Auth\Backend\DummyBackend;
use Smorken\Auth\Backend\LdapQueryBackend;
use Smorken\Service\Service;

class AuthService extends Service
{

    /**
     * Bind AuthHandler to 'auth' on App
     * AuthHandler will grab 'config' and 'session' from App as well as the full url of the current
     * page
     */
    public function load()
    {
        include_once __DIR__ . '/../../helpers.php';
        $this->app->instance(
            $this->getName(),
            function ($c) {
                $f = $this->getFactory();
                return new \Smorken\Auth\AuthHandler($f, $c['config'], $c['session'], $c['url']);
            }
        );
    }

    public function start()
    {
        $this->name = 'auth';
    }

    protected function getFactory()
    {
        $factory = new Factory();
        $factory->set(AuthProxyBackend::class, new AuthProxyBackend());
        $factory->set(DbModelBackend::class, new DbModelBackend());
        $factory->set(DummyBackend::class, new DummyBackend());
        $factory->set(LdapQueryBackend::class, new LdapQueryBackend());
        return $factory;
    }
} 
