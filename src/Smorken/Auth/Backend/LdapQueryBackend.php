<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/20/16
 * Time: 12:23 PM
 */

namespace Smorken\Auth\Backend;

use LdapQuery\LdapQuery;

/**
 * Class LdapQueryBackend
 * @package Smorken\Auth\Backend
 * Use LdapQuery as the backend
 * auth.php config file has the following options:
   'backend' => [
    'class' => '\Smorken\Auth\Backend\LdapQueryBackend',
    'options' => [
        'ldap_provider' => 'LdapQuery\Opinionated\ActiveDirectory',
        'provider_options' => [
            'bind_filter' => '%s@domain.edu',
            'host' => 'ldap.domain.edu',
            'base_dn' => 'ou=group,dc=domain,dc=org',
            'bind_user' => '',
            'bind_password' => '',
            'client_options' => [
                'start_tls' => true,
            ],
        ],
        'load_groups' => false,
        'username_attribute' => 'samaccountname',
        'id_attribute' => 'uid',
        'group_attribute' => 'memberof',
        'fields' => [
            'last_name' => 'sn',
            'first_name' => 'givenname',
            'email' => 'mail',
        ],
    ],
   ],
   'require_ssl' => true,
   'login_route' => 'login',
   'return_url_key' => 'returnUrl',
 * ...
 */
class LdapQueryBackend extends AbstractBackend implements IBackend
{

    /**
     * @var \LdapQuery\LdapQuery
     */
    protected $provider;

    protected $providerClass = '\LdapQuery\Opinionated\ActiveDirectory';

    protected $attributes = [
        'username' => 'samaccountname',
        'id' => 'uid',
        'groups' => 'memberOf',
    ];

    /**
     * Authenticates the user
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        try {
            $res = $this->provider->authenticate($username, $password);
            if (!$res) {
                $this->errors[] = 'Invalid username or password.';
            }
            else {
                $this->user = $this->getByUsername($username);
            }
        }
        catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return $this->isAuthenticated();
    }

    public function getByUsername($username)
    {
        $user = $this->provider->findUser($username);
        $arr = $this->setInfoArray($user);
        return new $this->userClass($arr);
    }

    public function initOptions($options)
    {
        if (array_key_exists('ldap_provider', $options)) {
            $this->providerClass = $options['ldap_provider'];
        }
        $p = new $this->providerClass($options['provider_options']);
        $this->setProvider($p->getLdapQuery());
        $this->options = $options;
        $this->initAttributeOptions($options);
        parent::initOptions($options);
    }

    protected function initAttributeOptions($options)
    {
        $this->setAttributeMap('username', array_get($options, 'username_attribute', 'samaccountname'));
        $this->setAttributeMap('id', array_get($options, 'id_attribute', 'uid'));
        $this->setAttributeMap('groups', array_get($options, 'group_attribute', 'memberOf'));
    }

    protected function setAttributeMap($which, $value)
    {
        $this->attributes[$which] = $value;
    }

    protected function getAttributeMap($which)
    {
        return array_get($this->attributes, $which, null);
    }

    /**
     * @param \LdapQuery\LdapQuery $provider
     */
    public function setProvider(\LdapQuery\LdapQuery $provider)
    {
        $this->provider = $provider;
    }

    protected function createUserQuery()
    {
        $filters = $this->provider->getBackend()->getOptionByKey('filters', []);
        $pf = array_get($filters, 'person', []);
        $q = $this->provider->newQuery()->select([]);
        if (array_get($pf, 'category', false)) {
            $q->where(array_get($pf, 'category'), $this->provider->escape(array_get($pf, 'category_type', 'person')));
        }
        if (array_get($pf, 'samaccounttype', false)) {
            $q->where('samaccounttype', $this->provider->escape(array_get($pf, 'samaccounttype')));
        }
        return $q;
    }

    /**
     * Maps the ldap result into an array that can be used by IUser
     * Uses the fields from config/auth - backend['options']['fields']
     * @param \LdapQuery\Contracts\Model $info
     * @return array
     */
    protected function setInfoArray($info)
    {
        /*
        * in $options set the fields array with each value
        * as a field you want from active directory
        * If you have 'username' => 'samaccountname' it will set the $info['username'] = $infoCollection->samaccountname
        * refer to the ldap schema for which fields are available.
        */
        $infoA = array();
        if ($info) {
            $infoA[constant($this->userClass . '::ID_FIELD')] = $info->getAttribute($this->getAttributeMap('id'));
            $infoA[constant($this->userClass . '::USERNAME_FIELD')] = $info->getAttribute(
                $this->getAttributeMap('username')
            );
            if (!empty($this->options['fields'])) {
                foreach ($this->options['fields'] as $k => $field) {
                    if ($k === 'groups') {
                        $infoA[$k] = $this->getAllGroups($info->getAttribute($this->getAttributeMap('groups')));
                    } elseif ($k === 'primarygroup') {
                        $infoA[$k] = $this->getPrimaryGroup($info->distinguishedname);
                    } else {
                        $infoA[$k] = $info->$field;
                    }
                }
            } else {
                $infoA['groups'] = $this->getAllGroups($info->getAttribute($this->getAttributeMap('groups')));
            }
        }
        return $infoA;
    }

    /**
     * Parses the groups from the AD groups
     * @param $groups
     * @return array|string
     */
    protected function getAllGroups($groups)
    {
        $grps = '';
        if (!is_null($groups)) {
            if (!is_array($groups)) {
                $groups = explode(',', $groups);
            }
            foreach($groups as $ngroups) {
                $ngroupexp = explode(',', $ngroups);
                $groupstring = reset($ngroupexp);
                if (substr($groupstring, 0, 3) === 'CN=') {
                    $grps[] = substr($groupstring, 3);
                }
            }
        }
        return $grps;
    }

    /**
     * Gets the user's primary group from their DN
     * @param $dn
     * @return string
     */
    protected function getPrimaryGroup($dn)
    {
        $groups = explode(',', $dn);
        return substr($groups[1], '3');
    }
}