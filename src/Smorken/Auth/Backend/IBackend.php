<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:33 AM
 */

namespace Smorken\Auth\Backend;


interface IBackend {
    /**
     * Authenticates the user
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password);

    /**
     * Logs the user model in
     * @param \Smorken\Auth\Model\IUser $user
     * @return bool
     */
    public function login($user);

    /**
     * Reset user object
     * @return null
     */
    public function logout();

    /**
     * @param array $options
     * @return null
     */
    public function initOptions($options);

    /**
     * @return mixed
     */
    public function getRawResult();

    public function setRawResult($raw);

    /**
     * @return \Smorken\Auth\Model\IUser
     */
    public function getUser();

    /**
     * @return bool
     */
    public function isAuthenticated();

    /**
     * @return array
     */
    public function getErrors();
}