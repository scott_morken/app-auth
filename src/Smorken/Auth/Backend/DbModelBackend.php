<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/8/15
 * Time: 1:23 PM
 */

namespace Smorken\Auth\Backend;

use Mockery\CountValidator\Exception;

class DbModelBackend extends AbstractBackend implements IBackend
{

    /**
     * @var \Smorken\Db\Contracts\Models\Model
     */
    protected $provider;

    /**
     * @var \Smorken\Utils\Hash\Contract\Hash;
     */
    protected $hasher;

    /**
     * Map IUser to Db Model
     *
     * @var array
     */
    protected $columns = [
        'id'       => 'id',
        'username' => 'username',
        'password' => 'password',
    ];

    /**
     * Authenticates the user
     *
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        $hashpw = $this->getHasher()
                       ->hash($password);
        try {
            $info = $this->getProvider()
                         ->findByAttributes(
                             [
                                 $this->columns['username'] => $username,
                                 $this->columns['password'] => $hashpw,
                             ]
                         );
        } catch (Exception $e) {
            $this->errors[] = 'Unable to connect to the authentication server.';
        }
        if (!$info) {
            $this->errors[] = 'Invalid username or password.';
        }
        $infoArray = $this->setInfoArray($info);
        $this->user = new $this->userClass((array)$infoArray);
        return $this->isAuthenticated();
    }

    /**
     * @return \Smorken\Utils\Hash\Contract\Hash
     */
    public function getHasher()
    {
        return $this->hasher;
    }

    public function setHasher($hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * @return \Smorken\Db\Contracts\Models\Model
     */
    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider($model)
    {
        $this->provider = $model;
    }

    public function initOptions($options)
    {
        if (!$this->getProvider()) {
            $modelClass = array_key_exists('model', $options) ? $options['model'] : '\Smorken\Auth\Model\DbUser';
            $this->setProvider(new $modelClass());
        }
        if (!$this->getHasher()) {
            $hashClass = array_key_exists('hash_provider', $options) ? $options['hash_provider']
                : '\Smorken\Utils\Hash\Md5Hash';
            $salt = array_key_exists('hash_salt', $options) ? $options['hash_salt'] : '';
            $this->setHasher(new $hashClass($salt));
        }
        if (array_key_exists('columns', $options)) {
            $this->columns = array_merge($this->columns, $options['columns']);
        }
        $this->options = $options;
        parent::initOptions($options);
    }

    protected function setInfoArray($model)
    {
        $infoArray = [];
        if ($model) {
            $this->raw = $model;
            foreach ($this->columns as $localkey => $modelkey) {
                $infoArray[$localkey] = $model->$modelkey;
            }
        }
        return $infoArray;
    }
}
