<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 2:02 PM
 */

namespace Smorken\Auth\Backend;


class AbstractBackend
{
    /**
     * @var \Smorken\Auth\Model\IUser
     */
    protected $user;

    /**
     * @var array
     */
    protected $errors = array();

    protected $userClass = '\Smorken\Auth\Model\GenericUser';

    protected $raw;

    /**
     * Logs the user model in on the backend
     * @param \Smorken\Auth\Model\IUser $user
     * @return bool
     */
    public function login($user)
    {
        $this->user = $user;
        return true;
    }

    /**
     * reset user object
     */
    public function logout()
    {
        $this->user = new $this->userClass();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return \Smorken\Auth\Model\IUser
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = new $this->userClass();
        }
        return $this->user;
    }

    /**
     * Checks if the user model has an ID
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->getUser()->getId() ? true : false;
    }

    /**
     * @param array $options
     * @return null
     */
    public function initOptions($options)
    {
        if (isset($options['userClass'])) {
            $this->userClass = $options['userClass'];
        }
    }

    public function getRawResult()
    {
        return $this->raw;
    }

    public function setRawResult($raw)
    {
        $this->raw = $raw;
    }
} 