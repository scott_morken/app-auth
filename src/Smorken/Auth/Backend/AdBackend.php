<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/7/14
 * Time: 8:34 AM
 */

namespace Smorken\Auth\Backend;

/**
 * Class AdBackend
 *
 * @package Smorken\Auth\Backend
 *
 * LDAP/AD authentication backend
 * Uses adLDAP as the provider
 */
class AdBackend extends AbstractBackend implements IBackend
{
    /**
     * @var \Adldap\Adldap
     */
    protected $ad;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * Attempts to authenticate username and password against adLDAP
     * if successful, creates a user object from adLDAP
     *
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        try {
            $res = $this->ad->authenticate($username, $password);
            if (!$res) {
                $this->errors[] = 'Invalid username or password.';
            } else {
                $this->user = $this->getById($username);
            }
        } catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return $this->isAuthenticated();
    }

    /**
     * Looks up the username in adLDAP and maps the result into IUser
     *
     * @param string $identifier
     * @return \Smorken\Auth\Model\IUser
     */
    public function getById($identifier)
    {
        $infoArray = null;
        $info = $this->ad->user()
                         ->find($identifier);
        if ($info) {
            $converted = $this->convertToEntry($info);
            $this->raw = $converted;
            $infoArray = $this->setInfoArray($converted);
            $infoArray[constant($this->userClass . '::ID_FIELD')] = $identifier;
        }
        return new $this->userClass((array)$infoArray);
    }

    public function initOptions($options)
    {
        if (!$this->ad) {
            $ad = new \Adldap\Adldap($options);
        }
        $this->setAdProvider($ad);
        $this->options = $options;
        parent::initOptions($options);
    }

    /**
     * @param \Adldap\Adldap $ad
     */
    public function setAdProvider(\Adldap\Adldap $ad)
    {
        $this->ad = $ad;
    }

    protected function convertToEntry($info)
    {
        $converted = new \Adldap\Objects\User($info, $this->ad->getLdapConnection());
        return $converted;
    }

    /**
     * Parses the groups from the adLDAP groups
     *
     * @param $groups
     * @return array|string
     */
    protected function getAllGroups($groups)
    {
        $grps = '';
        if (!is_null($groups)) {
            if (!is_array($groups)) {
                $groups = explode(',', $groups);
            }
            foreach ($groups as $ngroups) {
                $ngroupexp = explode(',', $ngroups);
                $groupstring = reset($ngroupexp);
                if (substr($groupstring, 0, 3) === 'CN=') {
                    $grps[] = substr($groupstring, 3);
                }
            }
        }
        return $grps;
    }

    /**
     * Gets the user's primary group from their DN
     *
     * @param $dn
     * @return string
     */
    protected function getPrimaryGroup($dn)
    {
        $groups = explode(',', $dn);
        return substr($groups[1], '3');
    }

    /**
     * Maps the ldap result into an array that can be used by IUser
     * Uses the fields from config/auth - backend['options']['fields']
     *
     * @param $info
     * @return array
     */
    protected function setInfoArray($info)
    {
        /*
        * in $options set the fields array with each value
        * as a field you want from active directory
        * If you have 'username' => 'samaccountname' it will set the $info['username'] = $infoCollection->samaccountname
        * refer to the adLDAP docs for which fields are available.
        */
        $infoA = [];
        if (!empty($this->options['fields'])) {
            foreach ($this->options['fields'] as $k => $field) {
                if ($k == 'groups') {
                    $infoA[$k] = $this->getAllGroups($info->memberof);
                } elseif ($k == 'primarygroup') {
                    $infoA[$k] = $this->getPrimaryGroup($info->distinguishedname);
                } else {
                    $infoA[$k] = $info->$field;
                }
            }
        } else {
            //if no fields array present default to username and displayName
            $infoA[constant($this->userClass . '::USERNAME_FIELD')] = $info->samaccountname;
            $infoA['displayname'] = $info->displayName;
            $infoA['primarygroup'] = $this->getPrimaryGroup($info->distinguishedname);
            $infoA['groups'] = $this->getAllGroups($info->memberof);
        }
        return $infoA;
    }

}
