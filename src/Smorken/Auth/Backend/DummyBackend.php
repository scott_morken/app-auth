<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 10:37 AM
 */

namespace Smorken\Auth\Backend;

use Smorken\Auth\Model\GenericUser;

/**
 * Class DummyBackend
 * @package Smorken\Auth\Backend
 *
 * Fake authentication backend, allows login with any credentials
 */
class DummyBackend extends AbstractBackend implements IBackend
{
    /**
     * Fake authentication backend, always returns user model (true)
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        $this->raw = array('id' => 1, 'username' => $username);
        $this->user = new GenericUser(array('id' => 1, 'username' => $username));
        return $this->isAuthenticated();
    }
}