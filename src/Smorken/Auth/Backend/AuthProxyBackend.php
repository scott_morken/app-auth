<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/24/18
 * Time: 7:50 AM
 */

namespace Smorken\Auth\Backend;

use GuzzleHttp\Client;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Providers\Guzzle;

class AuthProxyBackend extends AbstractBackend implements IBackend
{

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Provider
     */
    protected $ap;

    protected $attributes = [
        'username' => 'username',
        'id'       => 'id',
    ];

    protected $data_attributes = [
        'groups' => 'groups',
    ];

    /**
     * Authenticates the user
     *
     * @param $username
     * @param $password
     * @return false|\Smorken\Auth\Model\IUser
     */
    public function authenticate($username, $password)
    {
        $response = $this->ap->authenticate($username, $password);
        if ($response->isAuthenticated()) {
            $this->convertResponseToUser($response);
            return true;
        }
        $this->errors[] = $response->message;
        return false;
    }

    public function initOptions($options)
    {
        if (!$this->ap) {
            $this->ap = new Guzzle(new Client(), $options);
        }
        parent::initOptions($options);
    }

    public function setAuthProvider(Provider $provider)
    {
        $this->ap = $provider;
    }

    protected function convertResponseToUser(Response $response)
    {
        $attrs = [];
        foreach ($this->attributes as $rkey => $lkey) {
            $attrs[$lkey] = $response->user->$rkey;
        }
        if ($response->user->data) {
            foreach ($this->data_attributes as $rkey => $lkey) {
                if (isset($response->user->data[$rkey])) {
                    $attrs[$lkey] = $response->user->data[$rkey];
                }
            }
        }
        $this->user = new $this->userClass($attrs);
    }
}
