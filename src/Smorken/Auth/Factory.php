<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/24/18
 * Time: 7:54 AM
 */

namespace Smorken\Auth;

use Smorken\Auth\Backend\IBackend;

class Factory
{

    protected $instances = [];

    /**
     * @param $cls
     * @return IBackend
     */
    public function get($cls)
    {
        if ($this->has($cls)) {
            return $this->instances[$cls];
        }
        throw new \InvalidArgumentException("$cls has not been instantiated.");
    }

    public function has($cls)
    {
        return isset($this->instances[$cls]);
    }

    public function set($cls, IBackend $obj)
    {
        $this->instances[$cls] = $obj;
    }
}
