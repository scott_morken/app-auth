<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 1:02 PM
 */

namespace Smorken\Auth;

use Smorken\Auth\Backend\IBackend;
use Smorken\Config\Config;
use Smorken\Session\SessionHandler;
use Smorken\Utils\Url;

class AuthHandler
{

    /**
     * @var bool whether to use the auth handler or not
     */
    protected $useHandler;
    /**
     * The return url to redirect to (isset config/auth - redirect is true)
     *
     * @var string
     */
    protected $returnUrl;
    /**
     * The current request url
     *
     * @var string
     */
    protected $currentUrl;
    /**
     * @var \Smorken\Config\Config
     */
    protected $config;
    /**
     * The provider for the authentication action
     *
     * @var \Smorken\Auth\Backend\IBackend
     */
    protected $backend;

    /**
     * @var \Smorken\Auth\Factory
     */
    protected $factory;

    /**
     * @var \Smorken\Session\SessionHandler
     */
    protected $session;

    /**
     * @var \Smorken\Auth\Model\IRaw
     */
    protected $raw;

    /**
     * @var \Smorken\Http\Url
     */
    protected $http;

    /**
     * @param \Smorken\Config\Config          $config
     * @param \Smorken\Session\SessionHandler $session
     * @param \Smorken\Http\Url               $http
     */
    public function __construct(Factory $factory, Config $config, SessionHandler $session, \Smorken\Http\Url $http)
    {
        $this->useHandler = $config['auth.authenticate'] !== null ? $config['auth.authenticate'] : true;
        $this->setFactory($factory);
        $this->setSession($session);
        $this->setConfig($config);
        $this->setHttp($http);
        $this->init();
    }

    /**
     * Authenticates the user against the backend
     * If authenticated, saves to session and if config/auth - redirect is true
     * automatically redirects to the return url
     *
     * @param               $username
     * @param               $password
     * @param IBackend|null $override_backend
     * @return false|Model\IUser
     */
    public function authenticate($username, $password, $override_backend = null)
    {
        $backend = $override_backend;
        if ($backend === null) {
            $backend = $this->getBackend();
        }
        $return = $this->getReturnUrl();
        $this->getSession()
             ->clear();
        $this->getSession()
             ->regenerate();
        $this->setReturnUrl($return);
        $result = $backend->authenticate($username, $password);
        if ($result === true) {
            $this->setBackend($backend);
            //$this->saveBackendToSession($backend);
            $this->saveUserToSession($this->user());
            $this->saveRawToSession($backend->getRawResult());
        } else {
            if (!$result && $override_backend === null) {
                $failovers = $this->getConfig()
                                  ->get('auth.failover', []);
                foreach ($failovers as $fClass => $options) {
                    $new_backend = $this->getBackendFromClassAndConfig($fClass, $options);
                    $r = $this->authenticate($username, $password, $new_backend);
                    if ($r) {
                        return $r;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @return \Smorken\Auth\Backend\IBackend
     */
    public function getBackend()
    {
        return $this->backend;
    }

    /**
     * @param \Smorken\Auth\Backend\IBackend $backend
     */
    public function setBackend($backend)
    {
        $this->backend = $backend;
    }

    /**
     * @param string $class
     * @param array  $config
     * @return IBackend
     */
    public function getBackendFromClassAndConfig($class, $config = [])
    {
        $backend = $this->getFactory()->get($class);
        $backend->initOptions($config);
        return $backend;
    }

    /**
     * @return \Smorken\Config\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param \Smorken\Config\Config $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->getBackend()
                    ->getErrors();
    }

    /**
     * @return \Smorken\Auth\Factory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    public function setFactory(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return \Smorken\Http\Url
     */
    public function getHttp()
    {
        return $this->http;
    }

    /**
     * @param \Smorken\Http\Url $http
     */
    public function setHttp(\Smorken\Http\Url $http)
    {
        $this->http = $http;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        if (!$this->returnUrl) {
            $key = $this->getConfig()
                        ->get('auth.return_url_key', 'returnUrl');
            if ($this->getSession()->$key) {
                $this->returnUrl = $this->getSession()->$key;
            }
        }
        return $this->returnUrl;
    }

    /**
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $old = $this->getReturnUrl();
        if (stripos(
            $this->getConfig()
                 ->get('auth.login_route', 'login'),
            $returnUrl
        )) {
            if ($old) {
                return;
            } else {
                $returnUrl = ''; //nothing defined, send to base route
            }
        }
        if ($this->getHttp()) {
            $returnUrl = $this->getHttp()
                              ->to($returnUrl);
        }
        $key = $this->getConfig()
                    ->get('auth.return_url_key', 'returnUrl');
        $this->getSession()->$key = $returnUrl;
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return \Smorken\Session\SessionHandler
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param \Smorken\Session\SessionHandler $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * Checks if the logout key exists in $_GET and if it is
     * logouts
     */
    public function handleLogout()
    {
        if (isset($_GET[$this->config->get('auth.logout_key', 'logout')]) && $this->isAuthenticated()) {
            $this->logout();
        }
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->getErrors()) > 0;
    }

    /**
     * Checks if there is an authenticated user
     *
     * @return bool
     */
    public function isAuthenticated()
    {
        if ($this->getBackend()
                 ->isAuthenticated() && $this->useHandler) {
            $this->loadRawFromSession();
            return true;
        }
        $this->loadUserFromSession();
        $this->loadRawFromSession();
        $is_auth = $this->getBackend()
                        ->isAuthenticated();
        if ($this->useHandler) {
            return $is_auth;
        } else {
            return true;
        }
    }

    /**
     * Destroys the session and logouts from the backend
     */
    public function logout()
    {
        $retUrl = $this->getReturnUrl();
        $this->getSession()
             ->clear();
        $this->getSession()
             ->regenerate();
        $this->getBackend()
             ->logout();
        $this->setReturnUrl($retUrl);
    }

    /**
     * Checks if the config/auth - require_ssl is true and
     * if the current location isn't https, returns true
     */
    public function needsSsl()
    {
        if ($this->useHandler && $this->config['auth.require_ssl']) {
            if (isset($_SERVER) && isset($_SERVER['SERVER_PROTOCOL'])) {
                if (!Url::isSecure($_SERVER)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the raw result from the backend
     *
     * @return mixed
     */
    public function raw()
    {
        return $this->raw;
    }

    /**
     * Gets the user
     *
     * @return Model\IUser
     */
    public function user()
    {
        return $this->getBackend()
                    ->getUser();
    }

    /**
     * Sets up the backend from config/auth - backend
     *
     * @throws AuthHandlerException
     */
    protected function createBackendFromConfig()
    {
        $bc = $this->config->get('auth.backend', []);
        $backendClass = isset($bc['class']) ? $bc['class'] : '\Smorken\Auth\Backend\DummyBackend';
        $options = isset($bc['options']) ? $bc['options'] : [];
        $backend = $this->getBackendFromClassAndConfig($backendClass, $options);
        $this->setBackend($backend);
        if (!$this->getBackend()) {
            throw new AuthHandlerException('Backend must be instantiated.');
        }
    }

    /**
     * @throws AuthHandlerException
     */
    protected function init()
    {
        $this->createBackendFromConfig();
        $this->initSessionFromConfig();
        $this->initRawFromConfig();
        $this->loadBackendFromSession();
    }

    protected function initRawFromConfig()
    {
        if ($this->config->get('auth.store_raw', false)) {
            $cls = $this->config->get('auth.raw_class', '\Smorken\Auth\Model\GenericRaw');
            $this->raw = new $cls;
        }
    }

    /**
     * Sets up the session
     * Sets namespace to config/auth - session['namespace'] is set or 'auth' if not
     */
    protected function initSessionFromConfig()
    {
        $sc = $this->config->get('auth.session', []);
        $namespace = isset($sc['namespace']) ? $sc['namespace'] : 'auth';
        $this->getSession()
             ->getAdapter()
             ->setNamespace($namespace);
    }

    /**
     * Gets the backend object from the session
     *
     * @return bool
     */
    protected function loadBackendFromSession()
    {
        $serial = $this->getSession()->backend;
        if ($serial) {
            $backend = unserialize($serial);
            $this->setBackend($backend);
        }
    }

    /**
     * Gets the raw object from the session
     * and stores it to the raw object
     */
    protected function loadRawFromSession()
    {
        if ($this->raw) {
            $rawserial = $this->getSession()->raw;
            if ($rawserial) {
                $raw = unserialize($rawserial);
                $this->raw = $raw;
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the user object from the session
     * and log the user in if exists
     *
     * @return bool
     */
    protected function loadUserFromSession()
    {
        $userserial = $this->getSession()->user;
        if ($userserial) {
            $user = unserialize($userserial);
            return $this->getBackend()
                        ->login($user);
        }
        return false;
    }

    /**
     * Saves the backend object to the session
     *
     * @param $backend \Smorken\Auth\Backend\IBackend
     */
    protected function saveBackendToSession($backend)
    {
        $serial = serialize($backend);
        $this->getSession()->backend = $serial;
    }

    /**
     * Saves the raw data to the session
     *
     * @param mixed $rawdata
     */
    protected function saveRawToSession($rawdata)
    {
        if ($this->raw) {
            $this->raw->set($rawdata);
            $rawserial = serialize($this->raw);
            $this->getSession()->raw = $rawserial;
        }
    }

    /**
     * Saves the user object to the session
     *
     * @param $user \Smorken\Auth\Model\IUser
     */
    protected function saveUserToSession($user)
    {
        $serialuser = serialize($user);
        $this->getSession()->user = $serialuser;
    }

}
