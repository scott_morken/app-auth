<?php
$auth->handleLogout();
if (!$auth->isAuthenticated()) {
    if ($auth->needsSsl()) {
        Smorken\Utils\Redirect::go(Smorken\Utils\Url::full($_SERVER, true), $auth->getConfig()->get('auth.logout', 'logout'));
    }
    $authenticated = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $post = filter_input_array(INPUT_POST, array(
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
        ));
        $authenticated = $auth->authenticate($post['username'], $post['password']);
    }
    if (!$authenticated) {
        require_once 'html.php';
        exit;
    }
}