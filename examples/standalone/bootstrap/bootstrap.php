<?php
include __DIR__ . '/../../../vendor/autoload.php';
$cloader = new \Smorken\Config\FileLoader(__DIR__ . '/../config'); //__DIR__ represents base path where config files are stored
$config = new \Smorken\Config\Config($cloader);

$sadapter = new \Smorken\Session\Adapter\SessionAdapter();
$session = new \Smorken\Session\SessionHandler($sadapter);

$auth = new \Smorken\Auth\AuthHandler($config, $session, \Smorken\Utils\Url::full($_SERVER));

require 'auth.php';