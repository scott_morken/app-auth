<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:02 AM
 */
/**
 * Pimple is our dependency injection container
 */
$container = new Pimple\Container();
/**
 * We create our app handler here
 * Yes, Pimple is Pimple and not a contract, bad juju but it's ok
 */
$app = Smorken\Application\App::getInstance($container);

/**
 * Adds the array from bootstrap/paths.php to $app->addPaths()
 */
$app->addPaths(require __DIR__ . '/paths.php');
/**
 * Starts up the app
 * This simply registers the core services and any additional
 * services that are in config/app.php - services
 */
$app->start();

/**
 * Defines the DEBUG constant, uses the config provider to lookup app.debug
 * in the default case, this will look at config/app.php - debug
 */
define('DEBUG', $app['config']['app.debug']);

/**
 * find out if logout was requested, if so, logout
 */
$app['auth']->handleLogout();

/**
 * If we are not authenticated?
 */
if (!$app['auth']->isAuthenticated()) {
    if ($app['auth']->needsSsl()) {
        /**
         * We need to be running over https, redirect
         */
        Smorken\Utils\Redirect::go(Smorken\Utils\Url::full($_SERVER, true), $app['config']->get('auth.logout', 'logout'));
    }
    $authenticated = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        /**
         * We have a $_POST request with our username and password, the
         * user must trying to authenticate.
         * Sanitize the input a bit...
         */
        $post = filter_input_array(INPUT_POST, array(
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
            'redirectTo' => FILTER_SANITIZE_URL,
        ));
        /**
         * Set our return url to the page we are trying to view so the automatic redirect
         * will send us back where we want to be (if config/auth - redirect is true)
         */
        $app['auth']->setReturnUrl($post['redirectTo']);
        $authenticated = $app['auth']->authenticate($post['username'], $post['password']);
    }
    /**
     * We are not authenticated, show the login page
     */
    if (!$authenticated) {
        require_once 'html.php';
        /**
         * Get us out of the application!  We don't want the user to accidentally fall through to
         * the protected page
         */
        exit;
    }
}

return $app;