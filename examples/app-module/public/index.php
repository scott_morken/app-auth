<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 2:26 PM
 */
require __DIR__ . '/../bootstrap/autoload.php';

$app = require_once __DIR__ . '/../bootstrap/start.php';

echo "Hello! " . $app['auth']->user()->getId() . ', you are logged in to the sample app project!';
?>
<pre>
    <?php var_dump($app['auth']->user()); ?>
</pre>
<div>
<a href="index.php?<?php echo $app['config']->get('auth.logout_key', 'logout'); ?>">Logout</a>
</div>