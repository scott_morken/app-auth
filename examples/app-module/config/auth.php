<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/7/14
 * Time: 12:10 PM
 */
return array(
    'backend' => array(
        'class' => '\Smorken\Auth\Backend\DummyBackend',
        /*'class' => '\Smorken\Auth\Backend\DummyBackend',
        'options' => array(
            'account_suffix' => '@domain.org',
            'domain_controllers' => array('server1.domain.org'),
            'base_dn' => 'ou=group,dc=domain,dc=org',
            'admin_username' => 'admin_user',
            'admin_password' => 'admin_pw',
            'real_primary_group' => true,
            'use_ssl' => true,
            'use_tls' => false,
            'recursive_groups' => false,
        )*/
    ),
    'failover' => array(
        /*'classname' => array(
            //options
        )*/
    ),
    'require_ssl' => false,
    'return_url_key' => 'returnUrl',
    'login_route' => 'login',
);